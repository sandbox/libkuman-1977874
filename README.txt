This is a module used in Community Media installs to allow users to pick a 
project before doing something like adding a show or an airing. It presents 
views exposed filters for searching projects. If a single project is found it 
forwards on to the expected action (like create airing). If multiple projects 
are found it gives links to create content for each result.

This was developed by Mark Libkuman for Manhattan Neighborhood Network.
